<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
        /* Remove the navbar's default margin-bottom and rounded borders */
        .navbar {
            margin-bottom: 0;
            border-radius: 0;
        }

        /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
        .row.content {height: 450px}

        /* Set gray background color and 100% height */
        .sidenav {
            padding-top: 20px;
            background-color: #f1f1f1;
            height: 100%;
        }

        /* Set black background color, white text and some padding */
        footer {
            background-color: white;
            color: #9d9d9d;
            padding: 15px;
            font-size: 12px;
            text-align: left;
        }

        /* On small screens, set height to 'auto' for sidenav and grid */
        @media screen and (max-width: 767px) {
            .sidenav {
                height: auto;
                padding: 15px;
            }
            .row.content {height:auto;}
        }
    </style>
</head>
<body>

<nav class="navbar navbar-default">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">

            <h1 style="color: red;"><img src="{{ asset('images/ebay_logo.png') }}" width="150" height="75">Checkout</h1>
        </div>
        <div class="col-sm-2">
            How do you like our checkout?
            <a href="">Tell us what you think</a>
        </div>

</nav>

<div class="container-fluid text-center">
    <div class="row content">
        <div class="col-sm-2 sidenav">
        </div>


        <div class="col-sm-8 text-left">
            <div class="col-sm-8">
                <h3>Pay With</h3>
                @if(session('success'))
                    <h4 class="alert alert-success text-center">{{ session('success') }}</h4>
                @endif
                <div class="panel panel-default">

                    <div class="panel-body">
                        <table class="table table-condensed">
                            <thead>
                            <tr>
                                <th colspan="12">To add a coupon, choose a different payment method.</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    <form action="{{'/coupon-save'}}" method="post" class="form-inline">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <div class="">
                                                <label> Enter Coupon</label>
                                                <input type="text" class="form-control" id="coupon_no" name="coupon_no" placeholder="Enter Coupon Number">
                                                <input type="submit" value="Apply" class="btn btn-default">
                                            </div>
                                            @if($errors->has('coupon_no'))
                                                <span style="color: red;">{{ $errors->first('coupon_no') }}</span>
                                            @endif
                                        </div>
                                    </form>

                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="panel panel-default">
                    <h3>Ship to</h3> <hr>
                    <div class="panel-body">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <label for="sel1">Country or Origin</label>
                                    <select class="form-control" id="sel1">
                                        <option>Select...</option>
                                        <option value="us">United State</option>
                                        <option value="uk">United Kingdom</option>
                                        <option value="ban">Bangladesh</option>
                                        <option value="ind">India</option>
                                        <option value="chin">China</option>
                                        <option value="rush">Rushiya</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <label class="disable">First Name</label>
                                    <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Enter First Name">
                                </div>
                                <div class="col-sm-6">
                                    <label class="disable">Last Name</label>
                                    <input type="text" class="form-control" id="first_name" name="last_name" placeholder="Enter Last Name">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <label class="disable">Street Address</label>
                                    <input type="text" class="form-control" id="street_address" name="street_address" placeholder="Enter Street Address">
                                </div>
                                <div class="col-sm-6">
                                    <label class="disable">Street Address (2)</label>
                                    <input type="text" class="form-control" id="street_address2" name="street_address2" placeholder="Enter Street Address (2)">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <label class="disable">City</label>
                                    <input type="text" class="form-control" id="city" name="city" placeholder="Enter city Name">
                                </div>
                                <div class="col-sm-4">
                                    <label for="sel1">State</label>
                                    <select class="form-control" id="sel1">
                                        <option>Select...</option>
                                        <option value="us">United State</option>
                                        <option value="uk">United Kingdom</option>
                                        <option value="ban">Bangladesh</option>
                                        <option value="ind">India</option>
                                        <option value="chin">China</option>
                                        <option value="rush">Rushiya</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <label class="disable">Zip Code</label>
                                    <input type="text" class="form-control" id="zip" name="zip" placeholder="Enter Zip">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <label class="disable">Phone Number</label>
                                    <input type="text" class="form-control" id="phone_number" name="phone_number" placeholder="Enter Phone Number">
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-offset-8 col-sm-10">
                                    <button type="reset" class="btn btn-active">Cancel</button>
                                    <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <h3> </h3>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table class="table table-condensed">
                            <thead>
                            <tr>
                                <td>
                                    <div class="col-sm-8">
                                        <div class="row">Item (1)</div>
                                        <div class="row">Shipping </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="row">$380.00 </div>
                                        <div class="row">free</div>
                                    </div>
                                </td>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    <div class="col-sm-8">
                                        <div class="row"><STRONG>Total Order</STRONG></STRO></div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="row"><strong>$380.00</strong></div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="col-sm-12">
                            <input type="button" class="btn btn-primary" value="Confirm and Pay">
                        </div>
                    </div>
                </div>
                <div>
                    <h5><img src="{{ asset('images/ebay_logo.png') }}" height="25" width="50">MONY BACK GUARANTEE</h5>
                </div>
            </div>
        </div>
        <div class="col-sm-2 side-nav"></div>


    </div>
</div>

<footer class="container-fluid">
    <div class="col-md-12 ">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <p>Copyright © 1995-2018 eBay Inc. All Rights Reserved
                <a href="#"> Accessibility-</a>
                <a href="#"> User agreement-</a>
                <a href="#"> Privacy-</a>
                <a href="#"> Cookies-</a>
                <a href="#"> Cookies-</a> and
                <a href="#"> AdChoice</a>
            </p></div>
        <div class="col-md-2">
            <a href="#"> <img src="{{ asset('images/norton.png') }}" width="80" height="50"></a>
        </div>
    </div>

</footer>

</body>
</html>