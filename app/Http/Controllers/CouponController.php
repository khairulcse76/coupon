<?php

namespace App\Http\Controllers;

use App\Coupon;
use Illuminate\Http\Request;

class CouponController extends Controller
{
    public function save_coupon(Request $request){
        $this->validate($request,[
            'coupon_no' => 'unique:coupons,coupon_no|required|max:100|min:2'
        ]);

        $data=new Coupon;

        $data->coupon_no=$request->coupon_no;

        $done=$data->save();
        if ($done){
            session()->flash('success', 'Coupon Successfully saved');
        }
        return back();
    }
}
